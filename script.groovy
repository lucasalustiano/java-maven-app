def test() {
    echo 'testing'
}

def build() {
    echo 'building'
    echo "building the app version ${params.VERSION}"
}

def deploy() {
    echo 'deploying'
    echo "printing credentials ${SERVER_CREDENTIALS}"
}

return this
